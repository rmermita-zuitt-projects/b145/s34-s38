const Course = require('../models/Course');

module.exports.addCourse = async (data) => {
	console.log(data)
	// User is an admin
	if (data.isAdmin) {

		// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
		// Uses the information from the request body to provide all the necessary information
		let newCourse = new Course({
			name : data.course.name,
			description : data.course.description,
			price : data.course.price
		});

		// Saves the created object to our database
		return newCourse.save().then((course, error) => {

			// Course creation successful
			if (error) {

				return false;

			// Course creation failed
		} else {

			return `Course added ${course}`;

		};

	});

	// User is not an admin
} else {
	return false;
};

};

// retrieving all courses

module.exports.getAllCourses = async () => {

	return Course.find({}).then(result => {

		return result
	})
}

// retrieval of active courses

module.exports.getAllActive = () => {
	
	return Course.find({isActive : true}).then(result => {

		return result
	})
}

// retrieval of a specific course

module.exports.getCourse = (reqParams) => {

	return Course.findById(reqParams.courseId).then(result => {

		return result
	})
}


//Update course

module.exports.updateCourse = (data) => {
	console.log(data);
	return Course.findById(data.courseId).then((result, err) => {
		if (data.payload.isAdmin === true) {

			result.name = data.updatedCourse.name,
			result.description = data.updatedCourse.description,
			result.price = data.updatedCourse.price

			return result.save().then((updatedCourse, err) => {
				if (err) {
					return false
				} else {
					return updatedCourse
				}
			})
		} else {
			return `Unauthorized user.`
		}
	})
}

module.exports.archiveCourse = (data) => {
	return Course.findById(data.courseId).then((result, err) => {
		if (data.payload.isAdmin === true) {
			console.log(data);
			console.log(result)
			result.isActive = !data.archivedCourse.isActive;

			return result.save().then((archiveCourse, err) => {
				if (err) {
					return false
				} else {
					return archiveCourse.isActive
				}
			})
		} else {
			return `Unauthorized user.`
		}
	})
}
