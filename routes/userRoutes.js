const express = require('express');
const router = express.Router();
const userController = require('../controllers/userControllers');
const auth = require('../auth');

//Checking email
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController))
});

//Registration
router.post('/register', (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send (resultFromController))
});

//Login
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send (resultFromController))
});

// Retrieve specific details

router.post("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	// Provides the user's ID for the getProfile controller method
	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));

});

// Enrolling
router.post('/enroll', auth.verify, (req, res) => {
	console.log('Hello')
	console.log(auth.decode(req.headers.authorization))
	let data = {
		userId : auth.decode(req.headers.authorization).id,
		courseId : req.body.courseId,
		payload : auth.decode(req.headers.authorization)
	}
	console.log(data)
	userController.enroll(data).then(resultFromController => res.send(resultFromController))
});

module.exports = router;
