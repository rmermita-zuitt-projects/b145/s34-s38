const mongoose = require('mongoose');
const userSchema = new mongoose.Schema({


	firstName : {
		type: String,
		required: [true, "Please input your first name"]
	},

	lastName : {
		type: String,
		required: [true, "Please input your last name"]
	},

	email : {
		type: String,
		required: [true, "Please input your valid email address"]
	},

	password : {
		type: String,
		required: [true, "Password must be 6-12 characters"]
	},

	isAdmin : {
		type: Boolean,
		default: false
	},

	mobileNo : {
		type: String,
		required: [true, "Input your mobile number"]
	},

	enrollments : [
		{
			courseId: {
				type: String,
				required: [true, "Please input the course ID"]
			},
			enrolledOn: {
				type: Date,
				default: new Date()
			},
			status: {
				type: String,
				default: 'enrolled'
			}
		}
	]
})

module.exports = mongoose.model('User', userSchema);