//[SECTION] Dependencies and Modules
	const express = require('express');
	const mongoose = require('mongoose');
	const cors = require('cors');
	const dotenv = require('dotenv'); 
	const userRoutes = require('./routes/userRoutes');
	const courseRoutes = require('./routes/courseRoutes');

//[SECTION] Environment Variables Setup
	//configure the application in order for it to recognize and identify the necessary components needed to build the app successfully. 
	dotenv.config();
	//extract the variables from the .env file.
	//verify the variable by displaying its value in the console. make sure to identify the origin of the component.
	const secret = process.env.CONNECTION_STRING;

//[SECTION] Server Setup
	const app = express();
	const port = process.env.PORT || 4000;
	app.use(express.json());
	app.use(express.urlencoded({extended: true}));
	app.use(cors());



//[SECTION] Database Connect
	mongoose.connect(secret, 
		{
			useNewUrlParser : true,
			useUnifiedTopology: true
		}
	);

	let db = mongoose.connection

		db.on('error', () => console.error.bind(console, 'error'))
		db.once('open', () => console.log('Successfully connected to MongoDB'))


//[SECTION] Server Routes
	app.use('/users', userRoutes);
	app.use('/courses', courseRoutes);

//[SECTION] Server Response
	app.get('/', (req, res) => {
		res.send('Hosted in Heroku')
	})
	app.listen(port, () => console.log(`Server is running at port ${port}`))
